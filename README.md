#Calculate the difference between two dates. 

###Author : Ke (Wallace) Tan

Description: 
===
. Create an application that can read in pairs of dates in the following format 
                                DD MM YYYY, DD MM YYYY

. Validate the input data, and compute the difference between the two dates in days.

. Output of the application should be of the form -
                                DD MM YYYY, DD MM YYYY, difference

. Where the first date is the earliest, the second date is the latest and the difference is the number of days.
. Input can be from a file, or from standard input.
. Provide test data to exercise the application.
. Add the overview of the selected solution as comments.
. Expose the above functionality as a REST API

Notes:
You should not make use of the Java libraries for date manipulation (for example Date, Calendar classes).
You can limit calculation on an input range of dates from 1900 to 2010

Deliverable:
An IDE importable java project including but not restricted to source files and test files.

Hypothesis 
====
* The Leap year has 366 days.
* Any invalid input will occur exception.
* The input will be a File. However, the structure is extensible for other input types.
* The REST endpoint will generate a JSON output.
* _LocalDate_ is only used in the testcase.
* The end date will not be included in the calculation.
* Assuming that the required date format "DD MM YYYY" implies that a leading zero is required for days/months less
than 10.

Design/Architecture 
====
* The application uses helper pattern. The main logic is inside a helper class called _DatesUtil_. This helper class could be injected into any web-application.
* The raw data will be imported via file and parsed into the domain objects ( _MyDate_, _Month_, _Year_) for calculation. The default data file (_dates.dat_) is enclosed in the resources folder.
* The Design will follow Domain Driven Design and Test Driven Design.
* The application comes with File parser only. However, it could be extensible to multiple parsers.
* The application will bootstrap an embedded tomcat to provide the REST endpoint (_http://localhost:7070/dates_). Given the main focus of this test is not the web application, the code of bootstrapping tomcat is not fully optimized.
* The user can specify the data file via args in command or parameter in the url.


System Requirements 
====
* Java 8 is required.
* Maven 3.3.1+ is required.
* This project is a simple maven project.

How to run it 
====
To build the application and run all tests, please execute the following command in the root directory

      mvn clean install

##### File Input: 
To run the application with the default data file, please execute the following command in the root directory
      
      mvn exec:java@fileInput
   
To run the application with the specific data file, please execute the following command in the root directory
 
      mvn exec:java@fileInput -Dexec.args="<absolutePathOfFile>"

This sample of output is below:

        01 01 1900, 02 01 1900, 1
        01 01 1900, 02 01 2000, 36525
        02 01 2010, 02 04 2010, 90


##### REST API:
 
 To bootstrap the application with embedded tomcat, please execute the following command in the root directory
  

    sh target/bin/webapp
    
 And then, you should be able to access the endpoint via calling the following link.
     
    
    http://localhost:7070/dates
    
 To run the web-application with the specific data file, please execute the following command in the root directory
  
      http://localhost:7070/dates?path=<absolutePathOfFile>
   
    
This sample of output is below:
    

    [
      {
        "start": {
          "day": 1,
          "month": "JANUARY",
          "year": 1900
        },
        "end": {
          "day": 2,
          "month": "JANUARY",
          "year": 1900
        },
        "differenceInDays": 1
      },
      {
        "start": {
          "day": 1,
          "month": "JANUARY",
          "year": 1900
        },
        "end": {
          "day": 2,
          "month": "JANUARY",
          "year": 2000
        },
        "differenceInDays": 36525
      },
      {
        "start": {
          "day": 2,
          "month": "JANUARY",
          "year": 2010
        },
        "end": {
          "day": 2,
          "month": "APRIL",
          "year": 2010
        },
        "differenceInDays": 90
      }
    ]
    
Auto-Test
====
There are 45 test cases. The coverage is near 100% excluding the embedded tomcat part.

![picture](testCoverage.png)