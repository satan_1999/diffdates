package com.test.domain;

import com.test.domain.Month;
import com.test.enums.MonthType;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MonthTest {

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn1() {
        assertThat(Month.daysByTraversalFromEarlierToLater(Month.newInstance(MonthType.JANUARY, false), Month.newInstance(MonthType.FEBRUARY, false), false), is(0));
    }

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn28() {
        assertThat(Month.daysByTraversalFromEarlierToLater(Month.newInstance(MonthType.JANUARY, false), Month.newInstance(MonthType.MARCH, false), false), is(28));
    }

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn29() {
        assertThat(Month.daysByTraversalFromEarlierToLater(Month.newInstance(MonthType.JANUARY, true), Month.newInstance(MonthType.MARCH, true), true), is(29));
    }
}
