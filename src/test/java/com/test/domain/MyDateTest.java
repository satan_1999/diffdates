package com.test.domain;

import junit.framework.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class MyDateTest {
    private static final java.time.LocalDate TODAY = LocalDate.of(2003, 2, 1);

    @Test
    public void testToString() {
        Assert.assertEquals(MyDate.MyDateBuilder.fromLocalDate(TODAY).build().toString(), "01 02 2003");
    }

    @Test
    public void testToInt() {
        Assert.assertEquals(MyDate.MyDateBuilder.fromLocalDate(TODAY).build().toInt(), 20030102);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTestDateBuilder_ShouldSetLeapFebTrue() {
        Assert.assertTrue(new MyDate.MyDateBuilder().day(29).month(2).year(2010).build().getMonth().isLeapFeb());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTestDateBuilder_ShouldThrowException() {
        new MyDate.MyDateBuilder().day(29).month(2).year(2009).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTestDateBuilder_ShouldThrowException_WhenTheDayIsNotOfRange() {
        new MyDate.MyDateBuilder().day(33).month(4).year(2010).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTestDateBuilder_ShouldThrowException_WhenTheYearIsNotOfRange() {
        new MyDate.MyDateBuilder().day(2).month(2).year(2011).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTestDateBuilder_ShouldThrowException_WhenTheYearIsEarlierThanMinYear() {
        new MyDate.MyDateBuilder().day(2).month(2).year(1899).build();
    }

    @Test
    public void testTestDateBuilder_ShouldSetIsLeapFebTrue() {
        MyDate date = new MyDate.MyDateBuilder().day(2).month(2).year(2008).build();
        Assert.assertTrue(date.getMonth().isLeapFeb());
    }

    @Test
    public void testTestDateBuilder_ShouldSetIsLeapFebFalse() {
        MyDate date = new MyDate.MyDateBuilder().day(2).month(2).year(2000).build();
        Assert.assertTrue(!date.getMonth().isLeapFeb());
    }

}
