package com.test.domain;

import junit.framework.Assert;
import org.junit.Test;

public class YearTest {


    @Test
    public void testDaysByTraversalFromEarlierToLater_ShouldReturn1826() {
        Assert.assertEquals(1826, Year.daysByTraversalFromEarlierToLater(new Year(2004), new Year(2010)));
    }

    @Test
    public void testDaysByTraversalFromEarlierToLater_ShouldReturn0_ForTheSameYear() {
        Assert.assertEquals(0, Year.daysByTraversalFromEarlierToLater(new Year(2004), new Year(2004)));
    }

    @Test
    public void testDaysByTraversalFromEarlierToLater_ShouldReturn0() {
        Assert.assertEquals(0, Year.daysByTraversalFromEarlierToLater(new Year(2004), new Year(2005)));
    }
}
