package com.test;

import com.test.command.DatesCommand;
import junit.framework.Assert;
import org.junit.Test;

import java.util.List;

public class FileExecutorTest {
    @Test
    public void execute_ShouldLoadTheDefaultFile() throws Exception {
        FileExecutor executor = new FileExecutor();
        List<DatesCommand> commands = executor.execute(new String[0]);
        Assert.assertEquals(3, commands.size());
        Assert.assertFalse(commands.stream().anyMatch(c -> c.getDifferenceInDays() == 0 ));
    }

    @Test
    public void execute_ShouldLoadTheDefaultFile_WhenArgsisNull() throws Exception {
        FileExecutor executor = new FileExecutor();
        List<DatesCommand> commands = executor.execute(null);
        Assert.assertEquals(3, commands.size());
        Assert.assertFalse(commands.stream().anyMatch(c -> c.getDifferenceInDays() == 0 ));
    }

    @Test
    public void execute_ShouldLoadTheSpecificFile() throws Exception {
        FileExecutor executor = new FileExecutor( );
        String datafileLocation = App.class.getClassLoader().getResource(".").getPath() + "/dates2.dat";
        List<DatesCommand> commands = executor.execute(new String[] {datafileLocation});
        Assert.assertEquals(1, commands.size());
        Assert.assertFalse(commands.stream().anyMatch(c -> c.getDifferenceInDays() == 0 ));
    }

}