package com.test.command;

import org.junit.Assert;
import org.junit.Test;

public class DatesCommandBuilderTest {

    @Test
    public void buildFromLine_Ok() throws Exception {
        DatesCommand command =  new DatesCommand.DatesCommandBuilder().buildFromLine("01 01 2000, 01 02 2000");
        Assert.assertNotNull(command.getStart());
        Assert.assertNotNull(command.getEnd());
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildFromLine_WhenTheStrIsInvalid1() throws Exception {
        new DatesCommand.DatesCommandBuilder().buildFromLine("01 01 2000");
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildFromLine_WhenTheStrIsInvalid2() throws Exception {
        new DatesCommand.DatesCommandBuilder().buildFromLine("01 01 2000 01 02 2000");
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildFromLine_WhenTheStrIsInvalid3() throws Exception {
        new DatesCommand.DatesCommandBuilder().buildFromLine("01 01 2000.01 02 2000");
    }

    @Test(expected = IllegalArgumentException.class)
    public void buildFromLine_WhenTheStrIsInvalid4() throws Exception {
        new DatesCommand.DatesCommandBuilder().buildFromLine("01 01 2000,");
    }

    @Test(expected = IllegalArgumentException.class)
    public void parse_ShouldThrowException_WhenTheStrIsInvalid1() throws Exception {
        new DatesCommand.DatesCommandBuilder().parse("01 01 111");
    }
    @Test(expected = IllegalArgumentException.class)
    public void parse_ShouldThrowException_WhenTheStrIsInvalid2() throws Exception {
        new DatesCommand.DatesCommandBuilder().parse("0101 2000");
    }
    @Test(expected = IllegalArgumentException.class)
    public void parse_ShouldThrowException_WhenTheStrIsInvalid3() throws Exception {
        new DatesCommand.DatesCommandBuilder().parse("01 012000");
    }
    @Test(expected = IllegalArgumentException.class)
    public void parse_ShouldThrowException_WhenTheStrIsInvalid4() throws Exception {
        new DatesCommand.DatesCommandBuilder().parse("01 01 20000");
    }
    @Test(expected = IllegalArgumentException.class)
    public void parse_ShouldThrowException_WhenTheStrIsInvalid5() throws Exception {
        new DatesCommand.DatesCommandBuilder().parse("JAN 01 20000");
    }

    @Test(expected = IllegalArgumentException.class)
    public void parse_ShouldThrowException_WhenTheStrIsInvalid6() throws Exception {
        new DatesCommand.DatesCommandBuilder().parse("1 1 20000");
    }

}