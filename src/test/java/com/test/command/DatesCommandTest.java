package com.test.command;

import com.test.domain.MyDate;
import junit.framework.Assert;
import org.junit.Test;

public class DatesCommandTest {

    @Test(expected = IllegalArgumentException.class)
    public void validate_ShouldThrowException_WhenStartIsNull() throws Exception {
        DatesCommand command = new DatesCommand();
        command.setEnd(new MyDate.MyDateBuilder().day(29).month(2).year(2009).build());
        command.validate();
    }

    @Test(expected = IllegalArgumentException.class)
    public void validate_ShouldThrowException_WhenEndIsNull() throws Exception {
        DatesCommand command = new DatesCommand();
        command.setStart(new MyDate.MyDateBuilder().day(29).month(2).year(2009).build());
        command.validate();
    }

    @Test(expected = IllegalArgumentException.class)
    public void validate_ShouldThrowException_WhenEndIsEarlierThanStart() throws Exception {
        DatesCommand command = new DatesCommand();
        command.setStart(new MyDate.MyDateBuilder().day(29).month(2).year(2009).build());
        command.setEnd(new MyDate.MyDateBuilder().day(1).month(2).year(2001).build());
        command.validate();
    }

    @Test
    public void validate_ShouldPrintOutInRightFormat() throws Exception {
        DatesCommand command = new DatesCommand();
        command.setStart(new MyDate.MyDateBuilder().day(20).month(2).year(2009).build());
        command.setEnd(new MyDate.MyDateBuilder().day(1).month(2).year(2010).build());
        command.setDifferenceInDays(100);
        Assert.assertEquals("20 02 2009, 01 02 2010, 100", command.toString());
    }

}