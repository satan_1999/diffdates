package com.test.helper;

import com.test.domain.MyDate;
import org.junit.Before;
import org.junit.Test;

import java.security.InvalidParameterException;
import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DatesHelperTest {
    private static final java.time.LocalDate TODAY = LocalDate.of(2003, 2, 1);
    private static final LocalDate TOMORROW = LocalDate.of(2003, 2, 2);
    private static final LocalDate FEB_27TH_2010 = LocalDate.of(2010, 2, 27);
    private static final LocalDate MARCH_2ND = LocalDate.of(2010, 3, 2);
    private static final LocalDate JAN_1ST_2001 = LocalDate.of(2001, 1, 1);
    private static final LocalDate JAN_1ST_2002 = LocalDate.of(2002, 1, 1);
    private static final LocalDate JAN_1ST_2004 = LocalDate.of(2004, 1, 1);
    private static final LocalDate JAN_1ST_2005 = LocalDate.of(2005, 1, 1);
    private static final LocalDate NOV_1ST_2001 = LocalDate.of(2001, 11, 1);
    private static final LocalDate NOV_30TH_2001 = LocalDate.of(2001, 11, 30);
    private static final LocalDate DEC_31ST_2001 = LocalDate.of(2001, 12, 31);

    private DatesHelper service;

    @Before
    public void setUp() throws Exception {
        service = new DatesHelper();
    }

    @Test(expected = InvalidParameterException.class)
    public void testDifferenceInDaysFrom_ThrowException_WhenEndEarlierThanStart() {
        service.differenceInDays(MyDate.MyDateBuilder.fromLocalDate(JAN_1ST_2002).build(), MyDate.MyDateBuilder.fromLocalDate(JAN_1ST_2001).build());
    }

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn1() {
        assertThat(service.differenceInDays(MyDate.MyDateBuilder.fromLocalDate(TODAY).build(), MyDate.MyDateBuilder.fromLocalDate(TOMORROW).build()), is(1));
    }

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn365_ForTheNormalYear() {
        assertThat(service.differenceInDays(MyDate.MyDateBuilder.fromLocalDate(JAN_1ST_2001).build(), MyDate.MyDateBuilder.fromLocalDate(JAN_1ST_2002).build()), is(365));
    }

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn364() {
        assertThat(service.differenceInDays(MyDate.MyDateBuilder.fromLocalDate(JAN_1ST_2001).build(), MyDate.MyDateBuilder.fromLocalDate(DEC_31ST_2001).build()), is(364));
    }

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn366_ForTheLeapYear() {
        assertThat(service.differenceInDays(MyDate.MyDateBuilder.fromLocalDate(JAN_1ST_2004).build(), MyDate.MyDateBuilder.fromLocalDate(JAN_1ST_2005).build()), is(366));
    }

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn2249() {
        assertThat(service.differenceInDays(MyDate.MyDateBuilder.fromLocalDate(JAN_1ST_2004).build(), MyDate.MyDateBuilder.fromLocalDate(FEB_27TH_2010).build()), is(2249));
    }

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn29() {
        assertThat(service.differenceInDays(MyDate.MyDateBuilder.fromLocalDate(NOV_1ST_2001).build(), MyDate.MyDateBuilder.fromLocalDate(NOV_30TH_2001).build()), is(29));
    }

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn60() {
        assertThat(service.differenceInDays(MyDate.MyDateBuilder.fromLocalDate(NOV_1ST_2001).build(), MyDate.MyDateBuilder.fromLocalDate(DEC_31ST_2001).build()), is(60));
    }

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn3() {
        assertThat(service.differenceInDays(MyDate.MyDateBuilder.fromLocalDate(FEB_27TH_2010).build(), MyDate.MyDateBuilder.fromLocalDate(MARCH_2ND).build()), is(3));
    }

    @Test
    public void testDifferenceInDaysFrom_ShouldReturn2192() {
        assertThat(service.differenceInDays(MyDate.MyDateBuilder.fromLocalDate(LocalDate.of(2004, 1, 1)).build(), MyDate.MyDateBuilder.fromLocalDate(LocalDate.of(2010, 1, 1)).build()), is(2192));
    }

}
