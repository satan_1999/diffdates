package com.test.dataParser;

import com.test.command.DatesCommand;
import junit.framework.Assert;
import org.junit.Test;

import java.util.List;

public class FileDataParserTest {

    @Test
    public void testRead_ShouldReturnAListWith3Elements() throws NoSuchFieldException, SecurityException {
        String path = getClass().getClassLoader().getResource(".").getPath();
        String filePath = path + "dates.dat";
        FileDataParser parser = new FileDataParser();
        parser.setFileName(filePath);
        List<DatesCommand> result = parser.parse();
        Assert.assertEquals(3, result.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRead_ShouldThrowIllegalArgumentException_WhenTheFilePathInvalid() throws NoSuchFieldException, SecurityException {
        String path = getClass().getClassLoader().getResource(".").getPath();
        String filePath = path + "wrongfile.dat";
        FileDataParser parser = new FileDataParser();
        parser.setFileName(filePath);
        parser.parse();
    }
}
