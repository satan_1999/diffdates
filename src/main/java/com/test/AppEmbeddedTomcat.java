package com.test;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;

import javax.servlet.ServletException;
import java.io.File;

public class AppEmbeddedTomcat {
    public static void main(String[] args) throws LifecycleException, InterruptedException, ServletException {
        try {
            String webappDirLocation = "src/main/webapp/";
            Tomcat tomcat = new Tomcat();

            //The port that we should run on can be set into an environment variable
            //Look for that variable and default to 8080 if it isn't there.
            String webPort = System.getenv("PORT");
            if(webPort == null || webPort.isEmpty()) {
                webPort = "7070";
            }

            tomcat.setPort(Integer.valueOf(webPort));

            StandardContext ctx = (StandardContext) tomcat.addWebapp("/", new File(webappDirLocation).getAbsolutePath());
            System.out.println("configuring app with basedir: " + new File("./" + webappDirLocation).getAbsolutePath());

            // Declare an alternative location for your "WEB-INF/classes" dir
            // Servlet 3.0 annotation will work
            File additionWebInfClasses = new File("target/classes");
            WebResourceRoot resources = new StandardRoot(ctx);
            resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes",
                    additionWebInfClasses.getAbsolutePath(), "/"));
            ctx.setResources(resources);

            tomcat.start();
            tomcat.getServer().await();



//            DatesService datesService = new DatesService();
//            dataParser parser = new FileDataParser();
//            List<DatesCommand> commands = null;
//            String datafileLocation;
//            if (args == null || args.length == 0) {
//                String path = App.class.getClassLoader().getResource(".").getPath();
//                datafileLocation = path + "/dates.dat";
//                // load employees information from file
//
//            } else {
//                System.out.println("Found args -- " + args[0]);
//                datafileLocation = args[0];
//            }
//            ((FileDataParser) parser).setFileName(datafileLocation);
//            commands = parser.parse();
//            commands.stream().forEach(command -> command.setDifferenceInDays(datesService.differenceInDays(command.getStart(), command.getEnd())));
//            commands.stream().forEach(command -> System.out.println(command.toString()));
        } catch (Exception e) {
            System.out.println("Found error -- " + e.getMessage());
        }
    }
}
