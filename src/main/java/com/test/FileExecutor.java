package com.test;

import com.test.dataParser.DataParser;
import com.test.dataParser.FileDataParser;
import com.test.command.DatesCommand;
import com.test.helper.DatesHelper;

import java.util.List;

public class FileExecutor {
    private static final String DEFAULT_DATES_DAT = "/dates.dat";
    private final DatesHelper datesService = new DatesHelper();
    private final DataParser parser = new FileDataParser();

    public List<DatesCommand> execute(String[] args) {
        String datafileLocation = generateFilePath(args);
        ((FileDataParser) parser).setFileName(datafileLocation);
        List<DatesCommand> commands = parser.parse();
        commands.forEach(command -> command.setDifferenceInDays(datesService.differenceInDays(command.getStart(), command.getEnd())));
        return commands;
    }

    private String generateFilePath(String[] args) {
        String datafileLocation;
        if (args == null || args.length == 0) {
            datafileLocation = App.class.getClassLoader().getResource(".").getPath() + DEFAULT_DATES_DAT;
        } else {
            System.out.println("Found args -- " + args[0]);
            datafileLocation = args[0];
        }
        return datafileLocation;
    }
}
