package com.test.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.test.FileExecutor;
import com.test.command.DatesCommand;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@WebServlet(
        name = "DatesServlet",
        urlPatterns = {"/dates"}
)
public class DatesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String[] argsArr = null;
        String path = req.getParameter("path");
        if (path != null) {
            List<String> args = Collections.singletonList(path);
            argsArr = args.toArray(new String[1]);
        }

        List<DatesCommand> commands = new FileExecutor().execute(argsArr);
        ObjectMapper mapper = new ObjectMapper();

        ServletOutputStream out = resp.getOutputStream();
        resp.setContentType("application/json");
        mapper.writeValue(out, commands);
        out.flush();
        out.close();
    }
}
