package com.test.enums;

public enum MonthType {
    JANUARY(31),
    FEBRUARY(28),
    MARCH(31),
    APRIL(30),
    MAY(31),
    JUNE(30),
    JULY(31),
    AUGUST(31),
    SEPTEMBER(30),
    OCTOBER(31),
    NOVEMBER(30),
    DECEMBER(31);
    private final int length;

    MonthType(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public int asNumber() {
        return ordinal() + 1;
    }

    public boolean equals(MonthType monthType) {
        return this.ordinal() == monthType.ordinal();
    }

    public static MonthType fromNumber(int month) {
        return MonthType.values()[month - 1];
    }


}
