package com.test;

import com.test.command.DatesCommand;

import java.util.List;

class App {

    public static void main(String[] args) {
        try {
            FileExecutor executor = new FileExecutor();
            List<DatesCommand> commands = executor.execute(args);
            commands.forEach(command -> System.out.println(command.toString()));
        } catch (Exception e) {
            System.out.println("Found error -- " + e.getMessage());
        }
    }

}
