package com.test.helper;

import com.test.domain.Month;
import com.test.domain.MyDate;
import com.test.domain.Year;
import com.test.enums.MonthType;

import java.security.InvalidParameterException;

public class DatesHelper {

    public int differenceInDays(MyDate earlier, MyDate later) {
        if (earlier.getYear().getNumericalYear() < later.getYear().getNumericalYear()) {
            return dayDifferenceAcrossYears(earlier, later);
        } else if (earlier.getYear().getNumericalYear() == later.getYear().getNumericalYear()) {
            return dayDifferenceInTheSameYear(earlier, later);
        } else {
            throw new InvalidParameterException("Later Year is earlier than Earlier Year");
        }
    }

    private int dayDifferenceInTheSameMonth(MyDate earlier, MyDate later) {
        return (later.getDay() - earlier.getDay());
    }

    private int dayDifferenceInTheSameYear(MyDate earlier, MyDate later) {
        int diffInDays = 0;
        if (!earlier.getMonth().equals(later.getMonth())) {
            diffInDays += dayDifferenceAcrossMonths(earlier, later);
        } else {
            diffInDays += dayDifferenceInTheSameMonth(earlier, later);
        }
        return diffInDays;
    }

    private int dayDifferenceAcrossYears(MyDate earlier, MyDate later) {
        int diffInDays = 0;
        diffInDays += Year.daysByTraversalFromEarlierToLater(earlier.getYear(), later.getYear());
        diffInDays += dayDifferenceInTheSameYear(earlier, new MyDate(31, MonthType.DECEMBER, earlier.getYear())) + 1;
        diffInDays += dayDifferenceInTheSameYear(new MyDate(1, MonthType.JANUARY, later.getYear()), later);
        return diffInDays;
    }

    private int dayDifferenceAcrossMonths(MyDate earlier, MyDate second) {
        int diffInDays = 0;
        diffInDays += Month.daysByTraversalFromEarlierToLater(earlier.getMonth(), second.getMonth(), earlier.getYear().isLeapYear());
        diffInDays += dayDifferenceInTheSameYear(earlier, new MyDate(earlier.getMonth().getLength(), earlier.getMonth(), earlier.getYear())) + 1;
        diffInDays += dayDifferenceInTheSameYear(new MyDate(1, second.getMonth(), second.getYear()), second);
        return diffInDays;
    }

}
