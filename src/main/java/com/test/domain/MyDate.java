package com.test.domain;

import com.test.enums.MonthType;
import org.springframework.util.Assert;

import java.time.LocalDate;

import static java.lang.String.format;

public class MyDate {
    private final int day;
    private final Month month;
    private final Year year;

    public MyDate(int day, Month month, Year year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(int day, MonthType month, Year year) {
        this.day = day;
        this.month = Month.newInstance(month, year.isLeapYear());
        this.year = year;
    }

    public Year getYear() {
        return year;
    }

    @Override
    public String toString() {
        return String.format("%s %s %d", leftPad(day), leftPad(month.getMonthType().asNumber()), year.getNumericalYear());
    }

    public int getDay() {
        return day;
    }

    public Month getMonth() {
        return month;
    }

    private String leftPad(int value) {
        return String.format((value < 10) ? "0%d" : "%d", value);
    }

    public boolean isEarlierThan(MyDate end) {
        return this.toInt() <= end.toInt();
    }

    public int toInt() {
        return Integer.valueOf(String.format("%s%s%s", year.getNumericalYear(), leftPad(day), leftPad(month.getMonthType().asNumber())));
    }

    public static class MyDateBuilder {
        private int day;
        private Month month;
        private Year yearObj;


        public MyDateBuilder day(int day) {
            this.day = day;
            return this;
        }

        public MyDateBuilder month(int numericalMonth) {
            validatedMonth(numericalMonth);
            this.month = Month.newInstance(MonthType.fromNumber(numericalMonth), false);

            return this;
        }

        public MyDateBuilder year(int year) {
            validatedYear(year);
            this.yearObj = new Year(year);
            return this;
        }

        public void validatedMonth(int month) {
            Assert.isTrue(month >= 1 && month <= 12, format("Expected month to fall within range 1-12, found '%s'", month));
        }

        public void validatedDayInMonth(int day, Month month) {
            Assert.isTrue(day >= 1 && day <= month.getLength(), format("Expected day to fall within the range specified by month %s " +
                    "(1-%d), found '%d'", month, month.getLength(), day));

        }

        public void validatedYear(int year) {
            Assert.isTrue(year >= 1900 && year <= 2010, format("Expected year to fall within range 1900-2010, found '%s'", year));
        }

        public static MyDateBuilder fromLocalDate(LocalDate date) {
            return new MyDateBuilder().day(date.getDayOfMonth()).month(date.getMonthValue()).year(date.getYear());
        }

        public MyDate build() {
            if (this.month.getMonthType().equals(MonthType.FEBRUARY)) {
                this.month.setLeapFeb(this.yearObj.isLeapYear());
            }

            validatedDayInMonth(day, month);
            return new MyDate(day, month, yearObj);
        }

    }
}
