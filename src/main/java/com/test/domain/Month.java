package com.test.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import com.test.enums.MonthType;

public class Month {
    private MonthType monthType;
    private boolean isLeapFeb = false;


    public int getLength() {
        if (monthType.equals(MonthType.FEBRUARY) && isLeapFeb) {
            return monthType.getLength() + 1;
        }
        return monthType.getLength();
    }

    public void setLeapFeb(boolean isLeapFeb) {
        this.isLeapFeb = isLeapFeb;
    }

    public boolean isLeapFeb() {
        return isLeapFeb;
    }

    @JsonValue
    public String toString(){
        return monthType.toString();
    }

    public MonthType getMonthType() {
        return monthType;
    }

    private void setMonthType(MonthType monthType) {
        this.monthType = monthType;
    }

    public static int daysByTraversalFromEarlierToLater(Month earlier, Month later, boolean isLeapYear) {
        int days = 0;
        Month next = earlier.next(isLeapYear);
        if (next.earlierThan(later) && next.earlierThan(MonthType.DECEMBER)) {
            days += next.getLength();
            days += daysByTraversalFromEarlierToLater(next, later, isLeapYear);
        }
        return days;
    }

    private Month next(boolean isLeapYear) {
        Month nextMonth = new Month();
        nextMonth.setMonthType(MonthType.fromNumber(this.monthType.asNumber() + 1));
        if (nextMonth.getMonthType().equals(MonthType.FEBRUARY)) {
            nextMonth.setLeapFeb(isLeapYear);
        }
        return nextMonth;
    }

    private boolean earlierThan(Month later) {
        return this.monthType.ordinal() < later.monthType.ordinal();
    }

    private boolean earlierThan(MonthType later) {
        return this.monthType.ordinal() < later.ordinal();
    }

    public boolean equals(Month later) {
        return this.monthType.ordinal() == later.monthType.ordinal();
    }

    public static Month newInstance(MonthType monthType, boolean isLeapYear) {
        Month newMonth = new Month();
        newMonth.setMonthType(monthType);
        if (monthType.equals(MonthType.FEBRUARY)) {
            newMonth.setLeapFeb(isLeapYear);
        }
        return newMonth;
    }
}
