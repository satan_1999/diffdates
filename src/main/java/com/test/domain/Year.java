package com.test.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;

public class Year {
    private final static int LEAP_YEAR = 366;
    private final static int NORMAL_YEAR = 365;
    private final int numOfDays;
    private final int numericalYear;

    public Year(int numericalYear) {
        this.numericalYear = numericalYear;
        this.numOfDays = generateNumOfDays(numericalYear);
    }
    private int getNumOfDays() {
        return numOfDays;
    }
    @JsonValue
    public int getNumericalYear() {
        return numericalYear;
    }

    public boolean isLeapYear() {
        return numOfDays == LEAP_YEAR;
    }

    public static int daysByTraversalFromEarlierToLater(Year earlier, Year later) {
        int days = 0;
        if (earlier.getNumericalYear() + 1 < later.getNumericalYear()) {
            days += earlier.next().getNumOfDays();
            days += daysByTraversalFromEarlierToLater(earlier.next(), later);
        }
        return days;
    }


    private static int generateNumOfDays(int year) {
        return ((year % 4 == 0) && (year % 100 > 0)) ? LEAP_YEAR : NORMAL_YEAR;
    }

    private Year next() {
        return new Year(this.getNumericalYear() + 1);
    }
}
