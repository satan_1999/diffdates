package com.test.dataParser;

import com.test.command.DatesCommand;

import java.util.List;

public interface DataParser {
    List<DatesCommand> parse();
}
