package com.test.dataParser;

import com.test.command.DatesCommand;
import org.springframework.util.Assert;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class FileDataParser implements DataParser{
    private final Logger log = Logger.getLogger(FileDataParser.class);
    private String fileName;

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private List<DatesCommand> readTeamFromFile(String fileName) {
        Assert.notNull(fileName, "The fileName must not be null");
        List<DatesCommand> commands = new ArrayList();
        BufferedReader in = null;

        try {
            File file = new File(fileName);
            Assert.isTrue(file.exists(), "File Path ["+fileName+"] is invalid");
            in = new BufferedReader(new FileReader(file));

            String line;
            while ((line = in.readLine()) != null) {
                DatesCommand datesCommand = new DatesCommand.DatesCommandBuilder().buildFromLine(line);
                commands.add(datesCommand);
            }
            log.info("Loaded Commands  " + commands.size());
            in.close();
        } catch (IOException e) {
            log.error("Failed to load file", e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.error("Failed to close file", e);
                }
            }
        }
        return commands;
    }



    @Override
    public List<DatesCommand> parse() {
        return readTeamFromFile(fileName);
    }
}
