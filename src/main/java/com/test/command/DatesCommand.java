package com.test.command;

import com.test.domain.MyDate;
import org.springframework.util.Assert;

import java.util.regex.Pattern;

import static java.lang.Integer.valueOf;

public class DatesCommand {
    private static final Pattern CORRECT_FORMAT = Pattern.compile("\\d\\d \\d\\d \\d\\d\\d\\d");
    private MyDate start;
    private MyDate end;
    private int differenceInDays;

    public MyDate getStart() {
        return start;
    }

    public void setStart(MyDate start) {
        this.start = start;
    }

    public MyDate getEnd() {
        return end;
    }

    public void setEnd(MyDate end) {
        this.end = end;
    }

    public int getDifferenceInDays() {
        return differenceInDays;
    }

    public void setDifferenceInDays(int differenceInDays) {
        this.differenceInDays = differenceInDays;
    }

    public void validate() {
        Assert.notNull(start, "The start date is null");
        Assert.notNull(end, "The end date is null");
        Assert.isTrue(start.isEarlierThan(end), "The start date is not earlier than the end date");
    }

    public String toString() {
        return start.toString() + DatesCommandBuilder.DATE_DELIMITER + " " + end.toString() + DatesCommandBuilder.DATE_DELIMITER + " " + getDifferenceInDays();
    }

    public static class DatesCommandBuilder {

        public static final String DATE_DELIMITER = ",";
        public static final String FIELD_DELIMITER = " ";

        public DatesCommand buildFromLine(String line) {
            DatesCommand command = new DatesCommand();
            String[] twoDatesInStr = line.split(DATE_DELIMITER);
            Assert.isTrue(twoDatesInStr.length == 2, "The line [" + line + "] is invalid, expected to be of form 'DD MM YYYY, DD MM YYYY'");
            command.setStart(parse(twoDatesInStr[0].trim()));
            command.setEnd(parse(twoDatesInStr[1].trim()));
            command.validate();
            return command;
        }

        public MyDate parse(String dateStr) {
            String[] fragments = validated(dateStr).split(FIELD_DELIMITER);
            return new MyDate.MyDateBuilder().day(valueOf(fragments[0])).month(valueOf(fragments[1])).year(valueOf(fragments[2])).build();
        }

        private String validated(String date) {
            if (isNull(date) || !isCorrectlyFormatted(date)) {
                throw new IllegalArgumentException(String.format("Expected to be of form '00 00 0000', found '%s'", date));
            }
            return date;
        }

        private boolean isNull(Object object) {
            return object == null;
        }

        private boolean isCorrectlyFormatted(String date) {
            return CORRECT_FORMAT.matcher(date).matches();
        }

    }

}

